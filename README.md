# UniGUI and Delphi CookBook #

![UniGUI.png](https://bitbucket.org/repo/9p8xa7k/images/3217782801-UniGUI.png)

[UniGUI](http://www.unigui.com/) is a web application framework for Delphi. If you are looking for information about UniGUI and how to use the framework and components, you should start at its own website.

The goal of this project is to show how to effectively use Delphi, UniGUI, and several other tools like [Spring4D](https://bitbucket.org/sglienke/spring4d) for creating practical solutions.

If possible, following the idea of a CookBook, each "recipe" will be small, targeting a very specific feature.
Eventually, we will also build a few bigger projects.

One secondary goal is to show that Delphi itself is a modern and powerful language, not only because of the improvements to Object Pascal but also to RAD Studio.

Of course, we will pick some of the best features and technologies from Delphi, in addition to some of the best community tools like Spring4D, DUnitX, RemObjects, and others.

A good start is how to use databases, the Delphi Way. It means, at least, how to use FireDAC.

Let's not forget that there are many projects already built for the Windows Desktop. Many companies are looking for the same application, but now targeting the Web. Many of them chose to migrate it by reprogramming everything in C# for .NET. After the successful release of UniGUI, that is no longer necessary.

The topic about how to migrate a VCL application to UniGUI will analyzed after providing enough recipes. It will be clear that it is possible to build migration tools and design a sound strategy for making that migration much easier.

# CookBooks #

## Databases ##

Several recipes will be focused on other aspects of the application, not the database. For this reason, we will start using Memory Tables, but FireDAC TFDMemTable, not the old TClientDataSet.

The next database will be SQLite.

We will also use databases like Interbase, FireBird, MySQL, Microsoft SQL Server (probably localDB), Oracle (probably Oracle Express), and a few others.

After looking at several databases, we will gain database-independence by using an Object Relational Mapping tool like [Devart EntityDAC](https://www.devart.com/entitydac/), [TMS Aurelius](https://www.tmssoftware.com/site/aurelius.asp?s=history), or [Spring4D Marshmallow](https://bitbucket.org/sglienke/spring4d).

Even if we start using FireDAC, the preferred data access technology for Delphi, we could use other technologies in some recipes.

Whenever possible we will provide VCL and UniGUI versions of the same recipe.

### FireDAC ###

1. **MemTable**: Typical RAD-style form with TFDMemTable and two grids and navigators as master/detail. It shows how to use memory tables with fields and persistent data. It also configures the second dataset as a detail using properties. Both applications, VCL and UniGUI, are very similar.

2. **MemTable using a DataModule**: This time, all datasets and data sources reside in a common data module. It shows that it is possible to share the logic of the application while targeting different platforms.

3. **LocalSQL**: How to take advantage of [FireDAC's support for SQLite](http://docwiki.embarcadero.com/RADStudio/XE7/en/Local_SQL_(FireDAC)) using the component TFDLocalSQL and executing queries in memory.

4. **SQLite**: Similar code as Project 2 but using a SQLite database.

5. **MSSQL localDB**: Similar to SQLite.